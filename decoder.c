/**************
	*
	*	this source code is written by Oleksii Malyi
	*	
	*   It's a simple software decryper just as na example
	*
**************/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "functions.h"

int main (int arg_q, char **argv) 
{
	FILE *filePointer = NULL;
	unsigned long lSize = 0U, processSize = 0U;
	char *sourceBuf = NULL, *processBuf = NULL;
	size_t result = 0U;
	unsigned actualKeyLength = 0U, *the_key = NULL;
	// according to the task requirement max key length is 12-numbers => so it needs to be repeated sequentially 3 times 
	const size_t shift = MAX_KEY_LEN * 4; 
	// it means consilience_counter[0] will be ignored further, it plans to use range [1...36] 
	int consilience_counter[(MAX_KEY_LEN * 4) + 1] = {0};
	char alphabet[ALPHAPET_SIZE] = {0}, most_frequent_char = 'e', most_freq_index = 4; // in case of english using
	int frequency_counter[ALPHAPET_SIZE] = {0};
	for (int i = 0; i < ALPHAPET_SIZE; ++i)
		alphabet[i] = i + 'a';
	//	shift = atoi(argv[2]);
	if (arg_q <= 1) 
	{ 
		fputs("there were entered any file to be analized\nset some text file as a command line argument\n", stderr); 
		exit(1);
	}
	
	filePointer = fopen ( argv[1], "rb" );
	if (filePointer == NULL) 
	{ 
		fputs("File opening error\n", stderr); 
		exit(2);
	}

	// obtain file size:
	fseek (filePointer , 0, SEEK_END);
	lSize = ftell (filePointer);
	rewind(filePointer);

	// allocate memory to contain the whole file:
	sourceBuf = (char*) malloc (sizeof(char)*lSize);
	if (sourceBuf == NULL) 
	{
		fputs("Memory allocation error\n", stderr); 
		exit(3);
	}

	// copy the file into the sourceBuf:
	result = fread(sourceBuf, 1, lSize, filePointer);
	if (result != lSize) 
	{
		fputs("File reading error\n", stderr); 
		exit(4);
	}

	/* the whole file is now loaded in the memory sourceBuf. */
	
	// terminate
	fclose(filePointer);
	processSize = countLetters(lSize, sourceBuf);

	fputc( '\n', stdout);
	fprintf(stdout, "source file size is %lu\n", lSize);
	fprintf(stdout, "actual quantity of letters is %lu\n", processSize);
	processBuf = (char*) malloc (sizeof(char)*processSize);
	if (processBuf == NULL) 
	{
		fputs("Memory allocation error\n", stderr); 
		exit(5);
	}

	if (lSize > processSize)
		copyLetters(lSize, processBuf, sourceBuf);
	else
	{	// this case handler could be developed additionally 
		fputs("unexpected source data\n", stderr);
		exit(6);
	}
//	print_string(lSize, sourceBuf);
//	print_string(processSize, processBuf);	

	countMatchInexes(shift, processSize, consilience_counter, processBuf);

	for (int i = 2; i <= MAX_KEY_LEN && actualKeyLength == 0; ++i)
		actualKeyLength = isKeyLength(processSize, shift, consilience_counter, i);
	
	if (!actualKeyLength)
	{	 
		fputs("Programe can not decode the file, probably input text has been encoded with uknown cypher\n", stderr);
		exit(7);
	}
	fprintf(stdout, "actual key length of the text is %i\n", actualKeyLength);
	// allocate memory to save key (all items)
	the_key = (unsigned*) malloc (sizeof(unsigned)*actualKeyLength);
	if (the_key == NULL) 
	{
		fputs("Memory allocation error\n", stderr); 
		exit(8);
	}

	fprintf(stdout, "the key word is\t");
	for (char i = 0, letter_index = 0, v = 0; i < actualKeyLength; ++i)
	{
		letter_index = findMostRepeatedletter(processBuf, processSize, frequency_counter, actualKeyLength, i);	
	//	(letter_index >= 4) ? (the_key[i] = (unsigned)(alphabet[letter_index] - 'e') ) : (the_key[i] = (unsigned)(ALPHAPET_SIZE + alphabet[letter_index] - 'e') );
		(letter_index >= most_freq_index) ? (the_key[i] = (unsigned)(letter_index - most_freq_index) ) : (the_key[i] = (unsigned)(ALPHAPET_SIZE + letter_index - most_freq_index) );	
		fprintf(stdout, " %c", alphabet[*(the_key + i)]);
	}
	fprintf(stdout, "\n\n");
	
	for (int i = 0; i < actualKeyLength; ++i)
		exchangeLettersByKey(processBuf, processSize, the_key, actualKeyLength, i);
	fillArrayWithDecodedChars(sourceBuf, processBuf, lSize);
	
	print_string(lSize, sourceBuf);

	free(the_key);
	free(sourceBuf);
	free(processBuf);
	return 0;
}
