#include "functions.h"
unsigned long countLetters(unsigned long initialSize, char* array)
{
	unsigned long i = 0U, letter_counter = 0U;
	while(initialSize != i)
		if(isalpha(array[i++]) )
			++letter_counter; 

	return letter_counter;
}

void copyLetters(unsigned long fullSize, char* destination, char* source)
{
	for (unsigned long i = 0, n = 0; i < fullSize; ++i)
		if (isalpha( source[i]) )
			destination[n++] = tolower( source[i] );
}

void print_string(unsigned _size, char* ptr)
{
	for (int i = 0; i < _size; ++i)
		fputc( ptr[i], stdout);
	fputc( '\n', stdout);
}

void countMatchInexes(unsigned _shift, unsigned long _processSize, int*_consilience_counter, char*_processBuf)
{ 
	for (int tmp_shift = _shift; tmp_shift; tmp_shift--) 
	{
		//fprintf(stdout, "the loop with the key = %i\n", tmp_shift );
		_consilience_counter[tmp_shift] = 0;
		for (int i = 0, n = tmp_shift; n < _processSize; ++i, ++n)
		{
			if( n == _processSize )
				n = 0;  
			if( _processBuf[i] == _processBuf[n] )
				_consilience_counter[tmp_shift]++;
			//fprintf(stdout, "\t%i th iteration\t [ %c & %c ]\n", i, sourceBuf[i], sourceBuf[n]);

		}
	}
}


int isKeyLength(unsigned _processSize, unsigned _shift, int* matchInex, int expectedKeyLen)
{ 
	const float k = 0.0495;
	int result = 0, i = 0;

	for (i = expectedKeyLen; i <= _shift; i+=expectedKeyLen)
		 if (k < (float) ( ( (float) matchInex[i] ) / ( (float) _processSize) ) )
		 	result++;
		 else
		 	result--;

	//if(result > ((_shift / expectedKeyLen)) - 1 )
	if(result > 0)
		return expectedKeyLen;
	else
		return 0;
}

int findMostRepeatedletter(char* processArray, unsigned long _size, int* _frequency_counter, int stepLength, int row)
{
	char n = 0U;
	int most = 0;
	for (int i = 0; i < ALPHAPET_SIZE; ++i)
			_frequency_counter[i] = 0;

	for (int i = row; i < _size; i+=stepLength)
	{
		n = processArray[i] - 'a';
		_frequency_counter[n]++;
	}

	for (int i = 0; i < ALPHAPET_SIZE; ++i)
		if (most < _frequency_counter[i])
			most = _frequency_counter[i];

	for (int i = 0; i < ALPHAPET_SIZE; ++i)
		if (most == _frequency_counter[i])
			return i;
}

void exchangeLettersByKey(char* encodedArray, unsigned long _size, unsigned* key, int stepLength, int row)
{
	for (int i = row; i < _size; i+=stepLength)
	{
		if( (encodedArray[i] - key[row]) < 'a')
			encodedArray[i] = (encodedArray[i] - key[row] + ALPHAPET_SIZE);
		else
			encodedArray[i] = encodedArray[i] - key[row];		
	}
}

void fillArrayWithDecodedChars(char* fullArray, char* letterArray, unsigned long fullSize)
{
	for (unsigned long i = 0U, p = 0U, upper_flag = 0U; i < fullSize; ++i)
	{
		upper_flag = isupper(fullArray[i]);
		if (isalpha(fullArray[i]))
		{
			if(isupper(fullArray[i]))
				fullArray[i] = toupper(letterArray[p++]);
			if (islower(fullArray[i])) 
				fullArray[i] = letterArray[p++];
			
		}
		upper_flag = 0U;
	}
}