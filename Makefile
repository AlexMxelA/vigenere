EXECUTIVE_FILE = decoder.exe
COMPILE = gcc -c -std=c99
LINK = gcc -o
CLEAN = rm -f

$(EXECUTIVE_FILE): functions.o decoder.o
	$(LINK) $(EXECUTIVE_FILE) decoder.o functions.o

decoder.o: decoder.c
	$(COMPILE) decoder.c

functions.o: functions.c
	$(COMPILE) functions.c

clean:
	$(CLEAN) *.o
	$(CLEAN) decoder

