#ifndef FUNCTIONS_H
#define FUNCTIONS_H 0xFFU

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAX_KEY_LEN 12U
#define ALPHAPET_SIZE 26U

unsigned long countLetters(unsigned long, char*);
void copyLetters(unsigned long, char*, char *);
void print_string(unsigned int _size, char *ptr);
void countMatchInexes(unsigned _shift, unsigned long _processSize, int *_consilience_counter, char*_processBuf);
int isKeyLength(unsigned _size, unsigned _shift, int* matchInex, int expectedKeyLen);
int findMostRepeatedletter(char *processArray, unsigned long fullSize, int* _frequency_counter, int stepLength, int pos);
void exchangeLettersByKey(char* encodedArray, unsigned long _size, unsigned* key, int stepLength, int row);
void fillArrayWithDecodedChars(char* fullArray, char* letterArray, unsigned long fullSize);
#endif // FUNCTIONS_H
